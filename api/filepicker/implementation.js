try {
  if (typeof Cc === "undefined") { var Cc = Components.classes; }
  if (typeof Ci === "undefined") { var Ci = Components.interfaces; }
} catch (e) {}

var { ExtensionCommon } = ChromeUtils.import("resource://gre/modules/ExtensionCommon.jsm");

var filePicker = class extends ExtensionCommon.ExtensionAPI {
  getAPI(context) {
    return {
      filePicker: {
        async browseForFolder(windowId, windowTitle, folderPath) {
          return new Promise(resolve => {
            let window = context.extension.windowManager.get(windowId, context).window;
            let nsIFilePicker = Ci.nsIFilePicker;
            let fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

            try {
              fp.init(window, windowTitle, Ci.nsIFilePicker.modeGetFolder);
              try {
                if (folderPath) {
                  let localFile = Cc["@mozilla.org/file/local;1"].createInstance(
                    Ci.nsIFile);
                  localFile.initWithPath(folderPath);
                  fp.displayDirectory = localFile;
                }
              } catch (e) {}
              fp.open(r => {
                if (r !== Ci.nsIFilePicker.returnOK || !fp.file) {
                  resolve();
                } else {
                  resolve(fp.file.path);
                }
              });
            } catch (e) { resolve(); }
          });
        },

        async browseForApplication(windowId, windowTitle, appPath) {
          return new Promise(resolve => {
            let window = context.extension.windowManager.get(windowId, context).window;
            let nsIFilePicker = Ci.nsIFilePicker;
            let fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

            try {
              fp.init(window, windowTitle, Ci.nsIFilePicker.modeOpen);
              fp.appendFilters(Ci.nsIFilePicker.filterApps || Ci.nsIFilePicker
                .filterAll);
              try {
                if (appPath) {
                  let localFile = Cc["@mozilla.org/file/local;1"].createInstance(
                    Ci.nsIFile);
                  localFile.initWithPath(appPath);
                  localFile = localFile.parent;
                  fp.displayDirectory = localFile;
                }
              } catch (e) {}
              fp.open(r => {
                if (r !== Ci.nsIFilePicker.returnOK || !fp.file) {
                  resolve();
                } else {
                  resolve(fp.file.path);
                }
              });
            } catch (e) { resolve(); }
          });
        },

        async browseForFile(windowId, windowTitle, filePath, fileFilterExtensions, fileFilterDescription) {
          return new Promise(resolve => {
            let window = context.extension.windowManager.get(windowId, context).window;
            let nsIFilePicker = Ci.nsIFilePicker;
            let fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

            try {
              fp.init(window, windowTitle, Ci.nsIFilePicker.modeOpen);
              if (fileFilterExtensions) { fp.appendFilter(fileFilterDescription, fileFilterExtensions); }
              try {
                if (filePath) {
                  let localFile = Cc["@mozilla.org/file/local;1"].createInstance(
                    Ci.nsIFile);
                  localFile.initWithPath(filePath);
                  localFile = localFile.parent;
                  fp.displayDirectory = localFile;
                }
              } catch (e) {}
              fp.open(r => {
                if (r !== Ci.nsIFilePicker.returnOK || !fp.file) {
                  resolve();
                } else {
                  resolve(fp.file.path);
                }
              });
            } catch (e) { resolve(); }
          });
        }

      }
    };
  }
};
