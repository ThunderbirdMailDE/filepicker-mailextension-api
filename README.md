***Filepicker MailExtension API***

--------

### Features

This API implements some basic filepicker features, which can be used by your Mozilla Thunderbird (91+) MailExtension addons. At the moment this API is an "experiment", which means:
* that you (the addon coder) have to implement it accordingly to the guidelines for MailExtension experiments
* that users of the resulting addon have to allow "general access to Thunderbird and the whole computer" for the addon

Maybe this or a comparable API could be integrated into Thunderbird itself, to avoid that scary rights permit.

### Integration in your MailExtension

1. Copy the given files in api/filepicker/*.* to your addon.
2. Have a look into example.html, example.js and in manifest.json, how to use the API.


### Contribution

You are welcomed to contribute to this project by:
* creating [issues](https://gitlab.com/ThunderbirdMailDE/filepicker-mailextension-api/issues/) about problems or possible improvements


### Coders

* Alexander Ihrig (Maintainer)


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/filepicker-mailextension-api/LICENSE)