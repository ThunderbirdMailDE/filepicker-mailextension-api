// defaults for the example options dialog
let default_folderPath = "C:\\Downloads";
let default_appPath = "C:\\Downloads\\application.exe";
let default_filePath = "C:\\Downloads\\myfile.txt";

// onLoad listener to fill the example UI input fields with default values
// before using the filepicker dialogs
document.addEventListener("DOMContentLoaded", () => {
  document.getElementById("input_folderPath").value = default_folderPath;
  document.getElementById("input_appPath").value = default_appPath;
  document.getElementById("input_filePath").value = default_filePath;
});

// folderPath click listener
document.getElementById("button_folderPath").addEventListener("click", () => {
  browseFolderPath();
});
// appPath click listener
document.getElementById("button_appPath").addEventListener("click", () => {
  browseAppPath();
});
// filePath click listener
document.getElementById("button_filePath").addEventListener("click", () => {
  browseFilePath();
});

// Browse for a folder path
async function browseFolderPath() {
  // We need a windowId to pass it to the filepicker dialog.
  // So let's get the ID of the current window.
  let requestedWindow = await messenger.windows.getCurrent();
  let windowId = requestedWindow.id;

  // Read the latest folderPath from the input field (or from local storage for example):
  let folderPath = document.getElementById("input_folderPath").value;
  console.debug("Filepicker API: folderPath: " + folderPath);

  // Will be used later after the filepicker dialog.
  let newFolderPath = "";

  // This will be the filepicker dialogs window title.
  // Could be given from i18n files.
  let windowTitle = "Browse for folder";

  // selectedFolderPath will be filled with the resulting path from the filepicker dialog
  // ***** and this is the line, where you call / use the filePicker API:
  let selectedFolderPath = await messenger.filePicker.browseForFolder(windowId, windowTitle, folderPath);
  // *****
  console.debug("Filepicker API: selectedFolderPath: " + selectedFolderPath);

  // It could be possible that the resulting path is "undefined" because the user has canceled the filepicker
  // So you should only use the result if selectedFolderPath !== "undefined" as in the example below
  if (selectedFolderPath) {
    newFolderPath = selectedFolderPath;
  } else {
    // No new selected path, so do nothing and return.
    return;
  }
  console.debug("Filepicker API: newFolderPath: " + newFolderPath);

  // The resulting newFolderPath could than be used or for example be stored to local storage (and overwrite there the old "folderPath" value).
  // In this example the input field is filled with the newFolderPath.
  document.getElementById("input_folderPath").value = newFolderPath;
}

// Browse for an app path
async function browseAppPath() {
  // We need a windowId to pass it to the filepicker dialog.
  // So let's get the ID of the current window.
  let requestedWindow = await messenger.windows.getCurrent();
  let windowId = requestedWindow.id;

  // Read the latest appPath from the input field (or from local storage for example):
  let appPath = document.getElementById("input_appPath").value;
  console.debug("Filepicker API: appPath: " + appPath);

  // Will be used later after the filepicker dialog.
  let newAppPath = "";

  // This will be the filepicker dialogs window title.
  // Could be given from i18n files.
  let windowTitle = "Browse for an App";

  // selectedAppPath will be filled with the resulting path from the filepicker dialog
  // ***** and this is the line, where you call / use the filePicker API:
  let selectedAppPath = await messenger.filePicker.browseForApplication(windowId, windowTitle, appPath);
  // *****
  console.debug("Filepicker API: selectedAppPath: " + selectedAppPath);

  // It could be possible that the resulting path is "undefined" because the user has canceled the filepicker
  // So you should only use the result if selectedAppPath !== "undefined" as in the example below
  if (selectedAppPath) {
    newAppPath = selectedAppPath;
  } else {
    // No new selected path, so do nothing and return.
    return;
  }
  console.debug("Filepicker API: newAppPath: " + newAppPath);

  // The resulting newAppPath could than be used or for example be stored to local storage (and overwrite there the old "appPath" value).
  // In this example the input field is filled with the newAppPath.
  document.getElementById("input_appPath").value = newAppPath;
}

// Browse for a file path
async function browseFilePath() {
  // We need a windowId to pass it to the filepicker dialog.
  // So let's get the ID of the current window.
  let requestedWindow = await messenger.windows.getCurrent();
  let windowId = requestedWindow.id;

  // Read the latest filePath from the input field (or from local storage for example):
  let filePath = document.getElementById("input_filePath").value;
  console.debug("Filepicker API: filePath: " + filePath);

  // Will be used later after the filepicker dialog.
  let newFilePath = "";

  // This will be the filepicker dialogs window title.
  // Could be given from i18n files.
  let windowTitle = "Browse for a file";

  // This defines a filter for allowed file extensions in the filepicker.
  // Multiple file extensions have to be separated by semicolon!
  // Leave it undefined, if you don't want a filter
  let fileFilterExtensions = "*.txt;*.eml";
  // Leave the Description undefined, if you don't want a file filter description
  let fileFilterDescription = "Text- & Mail files";
  // The Description + FilterExtensions will be displayed as "Text- & Mail files (*.txt;*.eml)" in the file picker dialog.

  // selectedFilePath will be filled with the resulting path from the filepicker dialog
  // ***** and this is the line, where you call / use the filePicker API:
  let selectedFilePath = await messenger.filePicker.browseForFile(windowId, windowTitle, filePath, fileFilterExtensions, fileFilterDescription);
  // *****
  console.debug("Filepicker API: selectedFilePath: " + selectedFilePath);

  // It could be possible that the resulting path is "undefined" because the user has canceled the filepicker
  // So you should only use the result if selectedFilePath !== "undefined" as in the example below
  if (selectedFilePath) {
    newFilePath = selectedFilePath;
  } else {
    // No new selected path, so do nothing and return.
    return;
  }
  console.debug("Filepicker API: newFilePath: " + newFilePath);

  // The resulting newFilePath could than be used or for example be stored to local storage (and overwrite there the old "filePath" value).
  // In this example the input field is filled with the newFilePath.
  document.getElementById("input_filePath").value = newFilePath;
}
